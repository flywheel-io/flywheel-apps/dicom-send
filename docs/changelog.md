# Changelog

## 4.0.0

Nate: `storescu` was becoming challening for a few reasons:

1. Little visibility into failures
    a. No specific discernable return codes
    b. Particularly when there is a TLS/SSL failure, no indication of why
2. Need to call `storescu` individually to track files that failed
    a. Need to associate for each file which can be time consuming

There is an alternative `pynetdicom` which provides the same service, but
has an interface in python.

I rewrote to this, as well as refactored a lot of the old code, thoroughly
tested, typed, and enabled linting, bringing this gear up to current standards.
