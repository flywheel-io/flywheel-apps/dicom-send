<!-- markdownlint-disable-file -->
# Release notes

## 4.1.0

### Enhancements

* Add support for running without inputs at acquisition level
  * Allows for gear runs at acquisition level

### Maintenance

* Updates to CI pipeline

## 4.0.0

### Enhancements

* Rewrite the gear using `pynetdicom`:
  * Better logging
  * Better errors
  * Better performance due to bulk associations

### Maintenance

* Full linting, typing, vulnerability changing
* Much improved testing (30% cc -> 90%)

## 3.0.2

### Enhancements

* Transmit dicoms in ProcessPoolExecutor for performance improvement.

## 3.0.0

### Enhancements

* Expose TLS options from storescu, TLS, anonymous TLS, key/cert files, and
  configuration values.
* Improve logging of storescu errors
* Add retry when downloading files (no file input)

### Maintenance

* Reorganize code base
* Change from SDK -> CoreClient
* Change from pydicom 2.1 -> 2.3 and fw-file
* Add linting, typing, etc. from flywheel qa-ci
* Add tests
